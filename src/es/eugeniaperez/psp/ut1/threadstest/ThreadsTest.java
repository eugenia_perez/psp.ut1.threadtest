
package es.eugeniaperez.psp.ut1.threadstest;

/**
* A number of subprocesses printing in different intervals of time. This class
* handles the execution of all the subprocesses.
*
* @author <a href="http:\\eugeniaperez.es">Eugenia Pérez Martínez</a>
* @mailto eugenia.perez.martinez@gmail.com
 */
public class ThreadsTest {
    
       /**
    * Entry point of the application.
    *
    * @param args
    */
    public static void main(final String[] args) {
          // create and name each subprocess
          SubprocessPrinter subprocess1 =
               new SubprocessPrinter("subprocess1");
          SubprocessPrinter subprocess2 =
               new SubprocessPrinter("subprocess2");
          SubprocessPrinter subprocess3 =
               new SubprocessPrinter("subprocess3");
          System.err.println("Starting subprocesses");
          //Start threads and make them ready
          subprocess1.start();
          subprocess2.start();
          subprocess3.start();
          System.out.println("Subprocesses started. Main finishing\n");
    }
    
    /**
    * Initializes a Thread.
    * @param g the Thread group
    * @param target The object whose run() method gets called
    * @param name The name of the new Thread
    * @param stackSize The desired stack size for the new thread, or zero to indicate that this parameter is to be ignored.
    */
    private void init(ThreadGroup g, Runnable target, String name, long stackSize) {
        //Here the implementation …
    }

}
