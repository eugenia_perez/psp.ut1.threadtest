package es.eugeniaperez.psp.ut1.threadstest;

/**
* Class which represents a thread
*
* @author <a href="http:\\eugeniaperez.es">Eugenia Pérez Martínez</a>
* @mailto eugenia.perez.martinez@gmail.com
*/
class SubprocessPrinter extends Thread {

    /**
    * Time in miliseconds while the thread will be sleeping.
    */
    private int sleepingTime = 0;

    /**
    * Assign a name to the subprocess calling 
    * the superclass constructor.
    * @param name Name of the thread
    */
    public SubprocessPrinter(String name) {
        super(name);
        // select the sleeping time between 0 and 5 seconds
        sleepingTime = (int) (Math.random() * 5000);
     }

    /**
    * The run method is what it will be executed by the new subprocess.
    */
    @Override
    public void run() {
       // put subprocess to sleep for the amount of 
       //time specified by sleepingTime
        try {
            System.out.println(getName() + 
                        " is going to be sleeping for " + 
                            sleepingTime + " miliseconds");

            Thread.sleep(sleepingTime);
        } catch (InterruptedException excepcion) {
            System.err.println(excepcion.getMessage());
        }
            // print subprocess name
            System.out.println(getName() +
                "finished its sleeping time");
        }
}
